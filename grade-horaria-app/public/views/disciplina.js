async function getDisciplinaView() {
    return {
        template: await importar('./views/disciplina.html'),
        data() {
            return {
                id: null,
                disciplina: {},
                visualizando: false,
                filters: this.$route.params,
                alertaOptions: criarAlertaOptions()
            };
        },
        mounted() {
            if (this.filters.id) {
                this.buscarDisciplina(this.filters.id)
                this.visualizando = this.filters.acao === 'visualizar';
            }
        },
        methods: {
            voltar() {
                this.$router.push('/');
            },
            buscarDisciplina(id) {
                buscarRegistro(URL_API + '/disciplina', id)
                    .then(response => this.disciplina = response.data);
            },
            salvarDisciplina() {
                let self = this;
                salvarRegistro(URL_API + '/disciplina', self.disciplina)
                    .then((response) => {
                        if (!self.filters.id) {
                            this.$router.push({name: 'disciplina.editar', params: {id: response.data.id}});
                        }
                    })
                    .catch(response => self.alertaOptions.mensagemAlerta = getErroFormatado(response));
            }
        }
    };
}
