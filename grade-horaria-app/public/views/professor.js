async function getProfessorView() {
    return {
        template: await importar('./views/professor.html'),
        data() {
            return {
                id: null,
                professor: {},
                visualizando: false,
                filters: this.$route.params,
                alertaOptions: criarAlertaOptions()
            };
        },
        mounted() {
            if (this.filters.id) {
                this.buscarProfessor(this.filters.id)
                this.visualizando = this.filters.acao === 'visualizar';
            }
        },
        methods: {
            voltar() {
                this.$router.push('/');
            },
            buscarProfessor(id) {
                buscarRegistro(URL_API + '/professor', id)
                    .then(response => this.professor = response.data);
            },
            salvarProfessor() {
                let self = this;
                salvarRegistro(URL_API + '/professor', self.professor)
                    .then((response) => {
                        if (!self.filters.id) {
                            this.$router.push({name: 'professor.editar', params: {id: response.data.id}});
                        }
                    })
                    .catch(response => self.alertaOptions.mensagemAlerta = getErroFormatado(response));
            }
        }
    };
}
