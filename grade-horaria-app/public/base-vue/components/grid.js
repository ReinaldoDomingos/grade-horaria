Vue.component('grid', {
    props: ['lista', 'atributos', 'atributosSelecionaveis', 'atributosCheckbox', 'funcao', 'funcaoEditar',
        'funcaoExcluir', 'funcaoSalvar', 'funcaoImportar', 'isModoVisualizar', 'escoderBototes'],
    template: `
    <div>
    <div class="caixa grid">
        <button v-show="funcaoEditar" @click="funcaoEditar()" class="btn btn-primary float-left" style="margin-bottom: 20px;">Adicionar</button>
        <button v-show="funcaoImportar" @click="funcaoImportar()" class="btn btn-primary float-left" style="margin-left:10px;margin-bottom: 20px;">Importar</button>
        <div class="table-responsive-xl">
            <table class="table table-hover table-striped">
                <tr class="row">
                    <th v-if="atributos" v-bind:class="classesColuna(atributo)" v-for="(atributo, index) in atributos">{{atributo.titulo}}</th>
                    <th v-if="atributos" v-bind:class="classesColuna(atributo)" v-for="(atributo, index) in atributosSelecionaveis">{{atributo.titulo}}</th>
                    <th v-bind:class="classesColuna(atributo)" v-for="atributo in atributosCheckbox">{{atributo.titulo}}</th>
                    <th v-if="atributos" v-bind:class="classesColuna()"  
                    v-show="!escoderBototes && atributos.length && (funcaoEditar || funcaoExcluir || isModoVisualizar)"
                    class="acoes-grid text-center">
                        Ações
                    </th>
                </tr>
                <tr class="row" v-for="(item, linha) in lista.content">
                    <td v-bind:class="classesColuna(atributo)" v-for="atributo in atributos">
                        <span v-show="!atributo.editavel">{{getValorAtributo(item, atributo)}}</span>
                        <caixa-de-numero v-show="atributo.editavel" :valor="item" :campo="atributo.coluna"></caixa-de-numero>
                    </td>
                    <td v-bind:class="classesColuna(atributo)" class="text-left" v-for="atributo in atributosSelecionaveis">
                        <caixa-de-selecao :on-selecionar="funcaoSalvar(item)" :valor="item" 
                              v-bind:campo="atributo.chaveObjeto" :lista="atributo.lista" 
                              chave-combo="this" campo-combo="nome" auto-complete="true">
                        </caixa-de-selecao>
                    </td>
                    <td v-bind:class="classesColuna(atributo)" v-for="atributo in atributosCheckbox">
                      <div class="lista-checkbox">
                          <div  v-for="(itemCheckbox, index) in item[atributo.coluna]" class="form-check">
                            <input v-bind:id="'chk-' + _uid + '-'+ linha + '-' + index" class="form-check-input" type="checkbox" v-model="itemCheckbox.checked" v-bind:disabled="escoderBototes"/>
                            <label v-bind:for="'chk-' + _uid +  '-'+ linha + '-' + index" class="form-check-label">
                              {{ itemCheckbox.valor }}</label>
                          </div>
                      </div>
                    </td>
                    <td v-bind:class="classesColuna()" v-show="!escoderBototes && atributos.length && (funcaoSalvar || funcaoEditar || funcaoExcluir || isModoVisualizar)"
                    class="acoes-grid">
                        <a v-show="isModoVisualizar" class="btn" @click="funcaoEditar(item.id, true)">
                            <i class="material-icons">visibility</i>
                        </a>
                        <a v-show="funcaoEditar" class="btn" @click="funcaoEditar(item.id)">
                            <i class="material-icons">edit</i>
                        </a>
                        <a v-show="funcaoSalvar" class="btn" @click="funcaoSalvar(item)()">
                            <i class="material-icons">check</i>
                        </a>
                        <a v-show="funcaoExcluir" class="btn" @click="funcaoExcluir(item)">
                            <i class="material-icons">clear</i>
                        </a>
                    </td>
                </tr>
            </table>
            <nav class="paginacao naoSelecionavel">
                <ul class="pagination">
                    <li v-show="lista.content && lista.content.length" @click="paginaAnterior()" v-bind:class="{disabled:lista.number===0}" class="page-item">
                        <span class="page-link">Anterior</span>
                    </li>
                    <li v-show="pagina > paginaInicial && pagina  <= paginaFinal" @click="irPagina(pagina)" 
                            class="page-item" v-for="pagina in lista.totalPages"
                        v-bind:class="{active: pagina === paginaAtual}">
                        <span class="page-link">{{pagina}}</span>
                    </li>
                    <li v-show="lista.content && lista.content.length" @click="proximaPagina()" v-bind:class="{disabled:lista.totalPages===0 || paginaAtual === lista.totalPages}"
                        class="page-item">
                        <span class="page-link">Próximo</span>
                    </li>
                </ul>
                <span class="contagem">
                    Exibindo {{lista.totalElements ? (lista.number * lista.size + 1) : 0}} - {{(lista.number * lista.size) + lista.numberOfElements}} de {{lista.totalElements}}
                </span>
            </nav>
        </div>
    </div>
</div>
`,
    computed: {
        paginaAtual() {
            return this.lista.number + 1;
        },
        paginaInicial() {
            if (this.lista.totalPages < 10 || this.paginaAtual < 5) {
                return 0;
            }

            return this.paginaAtual - 5;
        },
        paginaFinal() {
            if (this.lista.totalPages < 10) {
                return this.lista.totalPages;
            }

            return this.paginaInicial + 10;
        }
    },
    methods: {
        getValorAtributo(item, atributo) {
            let atributos = atributo.coluna.split('.');


            let valor = item[atributos[0]];
            for (let i = 1; i < atributos.length; i++) {
                valor = valor[atributos[i]];
            }

            return valor;
        },
        classesColuna(atributo, valorPadrao) {
            let classes = {};

            valorPadrao = atributo ? valorPadrao : 'col-1';
            atributo = getValorOuValorPadrao(atributo, {});
            valorPadrao = getValorOuValorPadrao(valorPadrao, 'col-2');
            let valores = getValorOuValorPadrao(atributo.classes, [valorPadrao]);
            valores.forEach((valor) => classes[valor] = true);
            classes = isNotEmpty(valores) ? classes : {'col': true};

            return classes;
        },
        paginaAnterior() {
            if (this.lista.number > 0) {
                this.funcao(this.lista.number - 1);
            }
        },
        proximaPagina() {
            if (this.lista.number + 1 < this.lista.totalPages) {
                this.funcao(this.lista.number + 1);
            }
        },
        irPagina(pagina) {
            this.funcao(pagina - 1);
        }
    }
});