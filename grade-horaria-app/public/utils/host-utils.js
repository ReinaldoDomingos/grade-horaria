let HOST_API, URL_API;

async function definirUrlApi() {
    var response = await buscar('/env/host');
    HOST_API = response.data && response.data.host ? response.data.host : ('localhost:' + response.data.port);
    URL_API = 'http://' + HOST_API + '/gradehoraria-api'
}

definirUrlApi();