function getDiaDaSemana(diaDaSemana) {
    return filtroEnumValor(diaDaSemana, constantes.ENUMS.DIAS_DA_SEMANA);
}

function getDiaDaSemanaEnum(diaDaSemana) {
    let enumsFiltradas = filtroEnum(constantes.ENUMS.DIAS_DA_SEMANA, diaDaSemana);
    return enumsFiltradas.length ? enumsFiltradas[0] : null;
}
