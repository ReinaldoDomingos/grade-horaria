[‹ voltar ao README](./README.md)

# Notas de Release

Este arquivo descreve o conteúdo de cada release do projeto.

## 1.0.0

> ##### [FEATURE]: Criação da interface inicial e API
> ##### [FEATURE]: Adicionando funções de reparação
> ##### [FEATURE]: Adicionando restrição fraca de manter disciplinas no mesmo dia
> ##### [FEATURE]: Adicionando restrição fraca de disponibilidade de laboratório
> ##### [FEATURE]: Adicionando restrição fraca de não deixar mesma disciplina em dias consecutivos
> ##### [FEATURE]: Adicionando função de reparação
> ##### [FEATURE]: Adicionando função de avaliação
> ##### [FEATURE]: Adicionando Algoritmo básico de coloração de vértices
> ##### [FEATURE]: Adicionando restrições forte de professor e turma
> ##### [FEATURE]: Criação Algoritmo GRASP inicial
> ##### [FEATURE]: Criação estrutura banco de dados

[‹ voltar ao README](./README.md)