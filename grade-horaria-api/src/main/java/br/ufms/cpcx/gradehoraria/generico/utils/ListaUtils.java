package br.ufms.cpcx.gradehoraria.generico.utils;

import java.util.List;
import java.util.stream.Collectors;

public class ListaUtils {
    private ListaUtils() {
    }

    public static List<String> getListaSemRepeticoes(List<String> lista) {
        return lista.stream().distinct().sorted().collect(Collectors.toList());
    }
}
