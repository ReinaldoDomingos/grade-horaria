package br.ufms.cpcx.gradehoraria.gradehoraria.controller;

import br.ufms.cpcx.gradehoraria.gradehoraria.dto.RestricaoGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.RestricaoGradeHorariaProfessorDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.RestricaoGradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.enumaration.EDiaSemana;
import br.ufms.cpcx.gradehoraria.gradehoraria.filter.RestricaoGradeHorariaFilter;
import br.ufms.cpcx.gradehoraria.gradehoraria.service.RestricaoGradeHorariaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Controller
@RestController
@RequestMapping("/gradehoraria-api/gradeHoraria/{idGradeHoraria}/restricao")
public class RestricaoGradeHorariaController {

    @Autowired
    private RestricaoGradeHorariaService restricaoGradeHorariaService;

    @GetMapping
    public Page<RestricaoGradeHorariaDTO> buscar(@PathVariable("idGradeHoraria") Long idGradeHoraria, @RequestParam Map<String, String> filters) {
        return restricaoGradeHorariaService.buscarTodos(idGradeHoraria, RestricaoGradeHorariaFilter.of(filters));
    }

    @GetMapping("/porProfessor")
    public Page<RestricaoGradeHorariaProfessorDTO> buscarPorProfessor(@PathVariable("idGradeHoraria") Long idGradeHoraria, @RequestParam Map<String, String> filters) {
        return restricaoGradeHorariaService.buscarTodosPorFiltro(idGradeHoraria, RestricaoGradeHorariaFilter.of(filters));
    }

    @GetMapping("/todas")
    public List<RestricaoGradeHoraria> buscarTodas() {
        return restricaoGradeHorariaService.buscarTodos();
    }

    @GetMapping("/{id}")
    public RestricaoGradeHorariaDTO buscarPorId(@PathVariable("id") Long id) {
        return restricaoGradeHorariaService.buscarPorId(id);
    }

    @PostMapping("/professor/{idProfessor}")
    public void salvar(@PathVariable("idGradeHoraria") Long idGradeHoraria,
                       @PathVariable("idProfessor") Long idProfessor,
                       @RequestBody List<EDiaSemana> diaDaSemanas) {
        restricaoGradeHorariaService.salvarRestricoes(idGradeHoraria, idProfessor, diaDaSemanas);
    }

    @DeleteMapping("{id}")
    public void deletar(@PathVariable("id") Long id) {
        restricaoGradeHorariaService.deletar(id);
    }

    @PutMapping("{id}")
    public RestricaoGradeHorariaDTO alterar(@PathVariable("id") Long id, @RequestBody RestricaoGradeHorariaDTO restricaoGradeHorariaDTO) {
        return restricaoGradeHorariaService.alterar(id, restricaoGradeHorariaDTO);
    }
}
