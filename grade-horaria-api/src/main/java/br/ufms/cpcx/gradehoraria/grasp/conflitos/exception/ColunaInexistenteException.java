package br.ufms.cpcx.gradehoraria.grasp.conflitos.exception;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;

public class ColunaInexistenteException extends GenericException {
    public ColunaInexistenteException(String mensagem) {
        super(mensagem);
    }
}
