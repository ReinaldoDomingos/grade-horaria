package br.ufms.cpcx.gradehoraria.grasp.restricoes.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class GradeHorariaRestricaoDTO {
    private List<DiaRestricaoDTO> dias;

    public GradeHorariaRestricaoDTO() {
        this.dias = new ArrayList<>();
    }

    public DiaRestricaoDTO getDia(Integer dia) {
        List<DiaRestricaoDTO> diasFiltrados = getDiasFiltrados(Collections.singletonList(dia));

        if (diasFiltrados.isEmpty()) {
            DiaRestricaoDTO diaRestricaoDTO = new DiaRestricaoDTO(dia);
            this.dias.add(diaRestricaoDTO);

            return diaRestricaoDTO;
        } else {
            return diasFiltrados.get(0);
        }
    }

    public int getAvaliacaoDias(List<Integer> dias) {
        return getDiasFiltrados(dias).stream().mapToInt(DiaRestricaoDTO::getAvaliacaoNegativa).sum();
    }

    private List<DiaRestricaoDTO> getDiasFiltrados(List<Integer> dias) {
        return this.dias.stream()
                .filter(diaRestricaoDTO -> dias.contains(diaRestricaoDTO.getDia()))
                .collect(Collectors.toList());
    }
}
