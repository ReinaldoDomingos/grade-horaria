package br.ufms.cpcx.gradehoraria.arquivos.utils.xls;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.enumaration.EPeriodo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class TesteXls {
    public static void main(String[] args) {
        ExportarXLSUtils<String> exportarXLSUtils = new ExportarXLSUtils<>();

        List<ColunaXls> colunasCabecalho = new ArrayList<>();
        colunasCabecalho.add(new ColunaXls("Id", ETipoColunaXls.NUMERICO));

        HashMap<String, List<String>> colunas = new HashMap<>();

        ArrayList<String> valoresColuna = new ArrayList<>();
        valoresColuna.add(String.valueOf(1));

        colunas.put("Id", valoresColuna);

        exportarXLSUtils.setDados(colunasCabecalho, Collections.singletonList(""), colunas, EPeriodo.UNICO);

        exportarXLSUtils.salvarXLS("teste.xls");
    }
}