package br.ufms.cpcx.gradehoraria.gradehoraria.service;

import br.ufms.cpcx.gradehoraria.arquivos.business.ArquivosBO;
import br.ufms.cpcx.gradehoraria.arquivos.utils.xls.LeitorXlS;
import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.generico.utils.StringUtils;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.DisciplinaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.GradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.ProfessorDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.RestricaoGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.TurmaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.GradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Professor;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.RestricaoGradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.enumaration.EDiaSemana;
import br.ufms.cpcx.gradehoraria.gradehoraria.importacao.dto.EntidadesCadastroImportacaoDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.importacao.dto.ImportacaoGradeHorariaDTO;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Service
public class ImportacaoGradeHorariaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImportacaoGradeHorariaService.class);

    @Autowired
    private ArquivosBO arquivosBO;

    @Autowired
    private TurmaService turmaService;

    @Autowired
    private ProfessorService professorService;

    @Autowired
    private DisciplinaService disciplinaService;

    @Autowired
    private GradeHorariaService gradeHorariaService;

    @Autowired
    private RestricaoGradeHorariaService restricaoGradeHorariaService;

    @Autowired
    private DisciplinaGradeHorariaService disciplinaGradeHorariaService;

    @Transactional
    public GradeHorariaDTO importar(ImportacaoGradeHorariaDTO importacaoGradeHorariaDTO) throws IOException {
        GradeHorariaDTO gradeHorariaDTO = new GradeHorariaDTO();
        gradeHorariaDTO.setIsTeste(importacaoGradeHorariaDTO.getIsTeste());
        gradeHorariaDTO.setQuantidadeAulasPorTurno(importacaoGradeHorariaDTO.getQuantidadeAulasPorTurno());
        GradeHorariaDTO gradeHorariaSalvaDTO = gradeHorariaService.salvar(gradeHorariaDTO);

        String caminhoArquivoSalvo = arquivosBO.getURI(importacaoGradeHorariaDTO.getNomeArquivo());

        XSSFWorkbook planilha = LeitorXlS.getPlanilha(caminhoArquivoSalvo);
        List<String> linhas = LeitorXlS.getLinhas(planilha, 0);

        EntidadesCadastroImportacaoDTO entidadesCadastroImportacaoDTO = criarEntidadesCadastroImportacaoDTO(importacaoGradeHorariaDTO, linhas);

        GradeHoraria gradeHoraria = GradeHorariaDTO.toMapGradeHoraria(gradeHorariaSalvaDTO);

        cadastrarTurmasAutomatico(entidadesCadastroImportacaoDTO, gradeHoraria);
        cadastrarDisciplinasAutomatico(entidadesCadastroImportacaoDTO, gradeHoraria);
        cadastrarProfessoresAutomatico(entidadesCadastroImportacaoDTO, gradeHoraria);

        for (int i = 1; i < linhas.size(); i++) {
            List<String> linha = StringUtils.getListaDoTexto(linhas.get(i), "\t");
            String nomeTurma = linha.get(entidadesCadastroImportacaoDTO.getIndiceColunaTurma());
            Long idTurma = entidadesCadastroImportacaoDTO.getMapIdsTurmasCadastradas().get(nomeTurma);

            String nomeProfessor = linha.get(entidadesCadastroImportacaoDTO.getIndiceColunaProfessor());
            Long idProfessor = entidadesCadastroImportacaoDTO.getMapIdsProfessoresCadastradas().get(nomeProfessor);

            String nomeDisciplina = linha.get(entidadesCadastroImportacaoDTO.getIndiceColunaDisciplina());
            Long idDisciplina = entidadesCadastroImportacaoDTO.getMapIdsDisciplinasCadastradas().get(nomeDisciplina);

            String cargaHorariaSemanal = linha.get(entidadesCadastroImportacaoDTO.getIndiceColunaCargaHorariaSemananal());

            disciplinaGradeHorariaService.salvarCompleta(gradeHoraria.getId(), idDisciplina, idProfessor, idTurma, Integer.valueOf(cargaHorariaSemanal));
        }

        cadastrarRestricoesAutomatico(planilha, gradeHorariaSalvaDTO);

        return gradeHorariaSalvaDTO;
    }

    private void cadastrarRestricoesAutomatico(XSSFWorkbook planilha, GradeHorariaDTO gradeHorariaSalvaDTO) {
        if (planilha.getNumberOfSheets() > 1) {
            List<String> cabecalho = LeitorXlS.getCabecalho(planilha, 1);

            List<String> linhas = LeitorXlS.getLinhas(planilha, 1);

            linhas.remove(0);

            for (String linha : linhas) {
                List<String> valores = asList(linha.split("\t"));
                for (int i = 1; i < cabecalho.size(); i++) {
                    if (!valores.get(i).isEmpty()) {
                        String dia = cabecalho.get(i);
                        RestricaoGradeHoraria restricaoGradeHoraria = new RestricaoGradeHoraria();
                        restricaoGradeHoraria.setTipo(ETipoRestricao.DIAS_PROFESSOR_INDISPONIVEL);
                        restricaoGradeHoraria.setGradeHoraria(GradeHorariaDTO.toMapGradeHoraria(gradeHorariaSalvaDTO));
                        restricaoGradeHoraria.setProfessor(getProfessor(cabecalho, valores));
                        restricaoGradeHoraria.setDiaDaSemana(getDiaDaSemana(dia));
                        RestricaoGradeHorariaDTO restricaoGradeHorariaDTO = new RestricaoGradeHorariaDTO(restricaoGradeHoraria);
                        restricaoGradeHorariaService.salvar(restricaoGradeHorariaDTO);
                    }
                }
            }
        }
    }

    private EDiaSemana getDiaDaSemana(String dia) {
        return Arrays.stream(EDiaSemana.values()).filter(diaSemana -> diaSemana.getValor().equals(dia)).collect(Collectors.toList()).get(0);
    }

    private Professor getProfessor(List<String> cabecalho, List<String> valores) {
        return ProfessorDTO.toMapProfessor(professorService.buscarPorNome(valores.get(cabecalho.indexOf("Professor"))));
    }

    private void cadastrarTurmasAutomatico(EntidadesCadastroImportacaoDTO entidadesCadastroImportacaoDTO, GradeHoraria gradeHoraria) {
        List<String> turmas = entidadesCadastroImportacaoDTO.getTurmas();
        for (String nomeTurma : turmas) {
            String numeroNoNomeTurma = nomeTurma.replaceAll("[^\\d]", "");
            Integer semestre = !numeroNoNomeTurma.equals("") ? Integer.valueOf(numeroNoNomeTurma) : null;

            TurmaDTO turma = new TurmaDTO();
            turma.setNome(nomeTurma);
            turma.setSemestre(semestre);
            turma.setGradeHorariaTesteId(gradeHoraria.getId());

            TurmaDTO turmaSalva = turmaService.salvarOuGetPorNome(turma);

            entidadesCadastroImportacaoDTO.getMapIdsTurmasCadastradas().put(nomeTurma, turmaSalva.getId());
        }
    }

    private void cadastrarDisciplinasAutomatico(EntidadesCadastroImportacaoDTO entidadesCadastroImportacaoDTO, GradeHoraria gradeHoraria) {
        List<String> disciplinas = entidadesCadastroImportacaoDTO.getDisciplinas();
        for (String nomeDisciplina : disciplinas) {
            DisciplinaDTO disciplinaDTO = new DisciplinaDTO();
            disciplinaDTO.setNome(nomeDisciplina);
            disciplinaDTO.setGradeHorariaTesteId(gradeHoraria.getId());

            DisciplinaDTO disciplinaSalva = disciplinaService.salvarOuGetPorNome(disciplinaDTO);

            entidadesCadastroImportacaoDTO.getMapIdsDisciplinasCadastradas().put(nomeDisciplina, disciplinaSalva.getId());
        }
    }

    private void cadastrarProfessoresAutomatico(EntidadesCadastroImportacaoDTO entidadesCadastroImportacaoDTO, GradeHoraria gradeHoraria) {
        List<String> professores = entidadesCadastroImportacaoDTO.getProfessores();
        for (String nomeProfessor : professores) {
            ProfessorDTO professorDTO = new ProfessorDTO();
            professorDTO.setNome(nomeProfessor);
            professorDTO.setGradeHorariaTesteId(gradeHoraria.getId());

            ProfessorDTO professorSalvo = professorService.salvarOuGetPorNome(professorDTO);

            entidadesCadastroImportacaoDTO.getMapIdsProfessoresCadastradas().put(nomeProfessor, professorSalvo.getId());
        }
    }

    private EntidadesCadastroImportacaoDTO criarEntidadesCadastroImportacaoDTO(ImportacaoGradeHorariaDTO importacaoGradeHorariaDTO, List<String> linhas) {
        EntidadesCadastroImportacaoDTO entidadesCadastroImportacaoDTO = new EntidadesCadastroImportacaoDTO();

        entidadesCadastroImportacaoDTO.setIndicesColunasGradeHoraria(importacaoGradeHorariaDTO, linhas.get(0));
        entidadesCadastroImportacaoDTO.adicionarEntidadesCadastro(linhas);
        entidadesCadastroImportacaoDTO.limparRepeticoes();

        return entidadesCadastroImportacaoDTO;
    }

    public Map<String, Object> salvarArquivoDisciplinaXLS(MultipartFile arquivo) {
        Map<String, Object> retorno = new HashMap<>();

        try {
            retorno.put("colunasCabecalho", getColunasGradeHoraria(arquivo));
        } catch (GenericException g) {
            throw g;
        } catch (IOException e) {
            throw new GenericException("Erro ao ler arquivo xls");
        }

        String nomeArquivoSalvo = arquivosBO.salvarXLS(arquivo);
        retorno.put("nomeArquivo", nomeArquivoSalvo);

        return retorno;
    }

    public List<String> getColunasGradeHoraria(MultipartFile arquivo) throws IOException {
        Workbook planilha = LeitorXlS.getPlanilha(arquivo.getInputStream());
        return LeitorXlS.getCabecalho(planilha, 0);
    }
}
