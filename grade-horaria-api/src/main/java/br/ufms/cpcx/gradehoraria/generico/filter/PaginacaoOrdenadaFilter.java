package br.ufms.cpcx.gradehoraria.generico.filter;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static br.ufms.cpcx.gradehoraria.generico.utils.FilterUtils.getParametro;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class PaginacaoOrdenadaFilter {
    private int page;
    private int size;
    private Sort sort;

    public PaginacaoOrdenadaFilter(Map<String, String> parametros) {
        setPage(parametros);
        setSize(parametros);
        setSort(parametros);
    }

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }

    private void setSort(Map<String, String> parametros) {
        String sortStr = getParametro(parametros, "sort");
        String direcaoStr = getParametro(parametros, "direcaoStr");
        if (nonNull(sortStr)) {
            List<Order> ordens = new ArrayList<>();
            for (String atributo : sortStr.split(",")) {
                ordens.add(criarSortOrdem(atributo, direcaoStr));
            }
            this.sort = Sort.by(ordens);
        }
    }

    private void setPage(Map<String, String> parametros) {
        String pageStr = getParametro(parametros, "page");
        if (nonNull(pageStr)) {
            this.page = Integer.parseInt(pageStr);
        } else {
            this.page = 0;
        }
    }

    private void setSize(Map<String, String> parametros) {
        String sizeStr = getParametro(parametros, "size");
        if (nonNull(sizeStr)) {
            this.size = Integer.parseInt(sizeStr);
        } else {
            this.size = 5;
        }
    }

    public PageRequest getPageRequest() {
        if (isNull(this.sort)) {
            return PageRequest.of(this.getPage(), this.getSize());
        }

        return PageRequest.of(this.getPage(), this.getSize(), this.sort);
    }

    private static Order criarSortOrdem(String atributoSort, String direcaoSort) {
        Direction direction = getSortDirecaoDeString(direcaoSort);
        return new Order(direction, atributoSort);
    }

    private static Direction getSortDirecaoDeString(String directionStr) {
        return Objects.isNull(directionStr) ? Direction.ASC : Direction.fromString(directionStr);
    }
}
