package br.ufms.cpcx.gradehoraria.grasp.restricoes.dto;

import lombok.Data;

@Data
public class DiaRestricaoDTO {
    private Integer dia;
    private Integer avaliacaoNegativa;

    public DiaRestricaoDTO(Integer dia) {
        this.dia = dia;
        this.avaliacaoNegativa = 0;
    }
}
