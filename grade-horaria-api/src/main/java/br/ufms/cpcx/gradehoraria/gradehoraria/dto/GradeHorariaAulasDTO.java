package br.ufms.cpcx.gradehoraria.gradehoraria.dto;


import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.AulaDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GradeHorariaAulasDTO {
    private List<String> dias;
    private List<String> turmas;
    private List<String> professores;
    private List<List<AulaDTO>> aulas;
    private final String tituloColunaAulaTurma;
    private Integer quantidadeAulasPorTurno;

    public GradeHorariaAulasDTO() {
        this.aulas = new ArrayList<>();
        this.tituloColunaAulaTurma = "Semestre";
    }

    public void setDias(List<String> dias) {
        this.dias = dias;
    }

    public void setAulas(List<List<AulaDTO>> aulas) {
        this.aulas = aulas;
    }

    public List<String> getTurmas() {
        return turmas;
    }

    public void setTurmas(List<String> turmas) {
        this.turmas = turmas;
    }

    public List<String> getProfessores() {
        return professores;
    }

    public void setProfessores(List<String> professores) {
        this.professores = professores;
    }

    public List<List<AulaDTO>> getAulas() {
        return aulas;
    }

    public Integer getQuantidadeAulasPorTurno() {
        return quantidadeAulasPorTurno;
    }

    public void setQuantidadeAulasPorTurno(Integer quantidadeAulasPorTurno) {
        this.quantidadeAulasPorTurno = quantidadeAulasPorTurno;
    }

    public List<List<String>> getHorario() {
        return getHorario(false);
    }

    public List<List<String>> getHorario(boolean somenteProfessores) {
        List<List<String>> linhas = new ArrayList<>();

        List<String> linhaCabecalho = new ArrayList<>(Collections.singletonList(this.tituloColunaAulaTurma));
        linhaCabecalho.addAll(this.dias);

        linhas.add(linhaCabecalho);

        this.turmas.sort(Comparator.comparing(String::toString));

        for (String turma : this.turmas) {
            List<List<AulaDTO>> aulasTurmaPorDia = getListaAulasTurmaPorDia(turma);

            for (int numeroAula = 0; numeroAula < this.quantidadeAulasPorTurno; numeroAula++) {
                List<String> linha = new ArrayList<>();
                linha.add(turma);

                List<String> linhaAulasTurma = criarColunasVaziasTurmaEmCadaDia();

                adicionarLinhaHorarioAulasNaTurma(aulasTurmaPorDia, linhaAulasTurma, numeroAula, somenteProfessores);

                linha.addAll(linhaAulasTurma);
                linhas.add(linha);
            }
        }

        return linhas;
    }

    private void adicionarLinhaHorarioAulasNaTurma(List<List<AulaDTO>> aulasTurmaPorDia, List<String> linhaAulasTurma, int numeroAula, boolean somenteProfessores) {
        aulasTurmaPorDia.forEach(horarioAulas -> {
            horarioAulas.sort(Comparator.comparing(AulaDTO::getDisciplina));
            if (numeroAula < horarioAulas.size()) {
                AulaDTO aula = horarioAulas.get(numeroAula);
                int posDia = this.dias.indexOf(aula.getDia());

                if (posDia != -1) {
                    linhaAulasTurma.set(posDia, numeroAula < horarioAulas.size() ? getHorarioAulaParaImpressao(aula, somenteProfessores) : "");
                }
            }
        });
    }

    private List<List<AulaDTO>> getListaAulasTurmaPorDia(String turma) {
        List<List<AulaDTO>> aulasTurmaPorDia = new ArrayList<>();

        this.aulas.stream()
                .filter(horarioAulas -> !horarioAulas.isEmpty() && horarioAulas.get(0).getTurma().equals(turma))
                .forEach(aulasTurmaPorDia::add);
        return aulasTurmaPorDia;
    }

    private List<String> criarColunasVaziasTurmaEmCadaDia() {
        List<String> linhaAulasTurma = new ArrayList<>();

        for (int j = 0; j < this.dias.size(); j++) {
            linhaAulasTurma.add("");
        }
        return linhaAulasTurma;
    }

    private String getHorarioAulaParaImpressao(AulaDTO aula, boolean somenteProfessores) {

        if (somenteProfessores) {
            return aula.getProfessor();
        }

        return aula.getDisciplina() + "\nProfessor: " + aula.getProfessor();
    }
}