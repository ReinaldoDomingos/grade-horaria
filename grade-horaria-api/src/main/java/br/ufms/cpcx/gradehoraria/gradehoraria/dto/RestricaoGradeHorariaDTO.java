package br.ufms.cpcx.gradehoraria.gradehoraria.dto;

import br.ufms.cpcx.gradehoraria.gradehoraria.entity.RestricaoGradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.enumaration.EDiaSemana;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;
import lombok.Data;
import org.modelmapper.ModelMapper;

@Data
public class RestricaoGradeHorariaDTO {
    private Long id;

    private ETipoRestricao tipo;

    private GradeHorariaDTO gradeHoraria;

    private EDiaSemana diaDaSemana;

    private ProfessorDTO professor;

    public RestricaoGradeHorariaDTO(RestricaoGradeHoraria restricaoGradeHoraria) {
        this.id = restricaoGradeHoraria.getId();
        this.tipo = restricaoGradeHoraria.getTipo();
        this.professor = ProfessorDTO.fromProfessor(restricaoGradeHoraria.getProfessor());
        this.diaDaSemana = restricaoGradeHoraria.getDiaDaSemana();
        this.gradeHoraria = GradeHorariaDTO.fromGradeHoraria(restricaoGradeHoraria.getGradeHoraria());
    }

    public static RestricaoGradeHoraria toMapRestricaoGradeHoraria(RestricaoGradeHorariaDTO restricaoGradeHorariaDTO) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(restricaoGradeHorariaDTO, RestricaoGradeHoraria.class);
    }
}
