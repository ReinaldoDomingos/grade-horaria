package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorcargahoraria.dto;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Data
public class DiaCargaHorariaDTO {
    private Integer dia;
    private Integer cargaHorariaDisponivelTurma;
    private List<ProfessorCargaHorariaDTO> professores;

    public DiaCargaHorariaDTO(Integer dia, int cargaHorariaDisponivelTurma) {
        this.dia = dia;
        this.professores = new ArrayList<>();
        this.cargaHorariaDisponivelTurma = cargaHorariaDisponivelTurma;
    }

    public void adicionarEntidade(EntidadeDTO entidade) {
        ProfessorCargaHorariaDTO professor = getProfessor(entidade.getProfessor());

        if (isNull(professor)) {
            professor = new ProfessorCargaHorariaDTO(entidade.getProfessor());
            this.professores.add(professor);
        }

        this.cargaHorariaDisponivelTurma -= entidade.getCargaHorariaTotal();

        professor.adicionarEntidade(entidade);
        entidade.setDiaInicial(this.dia);
    }

    public int getCargaProfessor(String professor) {
        ProfessorCargaHorariaDTO professorCargaHorariaDTO = getProfessor(professor);

        return nonNull(professorCargaHorariaDTO) ? professorCargaHorariaDTO.getCargaHorariaTotalDia() : 0;
    }

    private ProfessorCargaHorariaDTO getProfessor(String professor) {
        List<ProfessorCargaHorariaDTO> professoresFiltrados = this.professores.stream()
                .filter(professorCargaHorariaDTO -> professorCargaHorariaDTO.getProfessor().equals(professor))
                .collect(Collectors.toList());

        return professoresFiltrados.isEmpty() ? null : professoresFiltrados.get(0);
    }
}
