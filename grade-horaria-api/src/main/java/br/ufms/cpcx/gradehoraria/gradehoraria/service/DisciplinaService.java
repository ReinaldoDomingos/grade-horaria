package br.ufms.cpcx.gradehoraria.gradehoraria.service;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.generico.filter.GenericFilter;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.DisciplinaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Disciplina;
import br.ufms.cpcx.gradehoraria.gradehoraria.repository.DisciplinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
public class DisciplinaService {

    @Autowired
    private DisciplinaRepository disciplinaRepository;

    public DisciplinaDTO salvar(DisciplinaDTO disciplinaDTO) {
        if (nonNull(disciplinaDTO.getCodigo()) && disciplinaRepository.existsDisciplinaByCodigo(disciplinaDTO.getCodigo())) {
            throw new GenericException("Disciplina já existe.");
        }

        return salvarDisciplina(DisciplinaDTO.toMapDisciplina(disciplinaDTO));
    }

    public DisciplinaDTO salvarOuGetPorNome(DisciplinaDTO disciplinaDTO) {
        if (disciplinaRepository.existsDisciplinaByNome(disciplinaDTO.getNome())) {
            return disciplinaRepository.findByNome(disciplinaDTO.getNome());
        }

        return salvarDisciplina(DisciplinaDTO.toMapDisciplina(disciplinaDTO));
    }

    public List<DisciplinaDTO> buscarTodas() {
        return disciplinaRepository.findAll().stream().map(DisciplinaDTO::new)
                .sorted(Comparator.comparing(DisciplinaDTO::getNome))
                .collect(Collectors.toList());
    }

    public Page<DisciplinaDTO> buscarTodas(GenericFilter filter) {
        return disciplinaRepository.findAll(filter.getPageRequest()).map(DisciplinaDTO::new);
    }

    public DisciplinaDTO buscarPorId(Long id) {
        return disciplinaRepository.findById(id).map(DisciplinaDTO::new).orElse(null);
    }

    public void deletar(Long id) {
        disciplinaRepository.deleteById(id);
    }

    public void deletarTodosPorGradeHorariaId(Long id) {
        disciplinaRepository.deleteAllByGradeHorariaTesteId(id);
    }

    public DisciplinaDTO alterar(Long id, DisciplinaDTO disciplinaDTO) {
        if (!id.equals(disciplinaDTO.getId()))
            throw new GenericException("Erro ao atualizar o registro.");

        return salvarDisciplina(DisciplinaDTO.toMapDisciplina(disciplinaDTO));
    }

    private DisciplinaDTO salvarDisciplina(Disciplina disciplina) {
        Disciplina disciplinaSalva = disciplinaRepository.save(disciplina);

        return new DisciplinaDTO(disciplinaSalva);
    }
}
