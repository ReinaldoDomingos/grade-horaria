package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorcargahoraria.dto;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProfessorCargaHorariaDTO {
    private final String professor;
    private List<EntidadeDTO> entidades;
    private Integer cargaHorariaTotalDia;

    public ProfessorCargaHorariaDTO(String professor) {
        this.professor = professor;
        this.cargaHorariaTotalDia = 0;
        this.entidades = new ArrayList<>();
    }

    public void adicionarEntidade(EntidadeDTO entidade) {
        this.entidades.add(entidade);

        int cargaHorariaTotal = entidade.getCargaHorariaTotal();

        this.cargaHorariaTotalDia += cargaHorariaTotal;
    }
}
