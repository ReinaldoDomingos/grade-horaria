package br.ufms.cpcx.gradehoraria.grasp.restricoes.dto;

import br.ufms.cpcx.gradehoraria.generico.enumaration.EStatus;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ESituacaoRestricao;
import br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration.ETipoRestricao;

public class Restricao {
    private EStatus status;
    private final ETipoRestricao tipo;
    private ESituacaoRestricao situacao;

    public Restricao(ETipoRestricao tipo) {
        this.tipo = tipo;
        this.status = EStatus.INATIVO;
        this.situacao = ESituacaoRestricao.NAO_APLICADO;
    }

    public EStatus getStatus() {
        return status;
    }

    public void setStatus(EStatus status) {
        this.status = status;
    }

    public ESituacaoRestricao getSituacao() {
        return this.situacao;
    }

    public void setSituacao(ESituacaoRestricao situacao) {
        this.situacao = situacao;
    }

    public ETipoRestricao getTipo() {
        return tipo;
    }
}
