package br.ufms.cpcx.gradehoraria.gradehoraria.service;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.generico.filter.GenericFilter;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.TurmaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Turma;
import br.ufms.cpcx.gradehoraria.gradehoraria.repository.TurmaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.nonNull;

@Service
public class TurmaService {

    @Autowired
    private TurmaRepository turmaRepository;

    public TurmaDTO salvar(TurmaDTO turmaDTO) {
        if (nonNull(turmaDTO.getCodigo()) && turmaRepository.existsTurmaByCodigo(turmaDTO.getCodigo()))
            throw new GenericException("Turma já existe.");

        return salvarTurma(turmaDTO);
    }

    public TurmaDTO salvarOuGetPorNome(TurmaDTO turmaDTO) {
        if (turmaRepository.existsTurmaByNome(turmaDTO.getNome()))
            return buscarPorNome(turmaDTO);

        return salvarTurma(turmaDTO);
    }

    private TurmaDTO buscarPorNome(TurmaDTO turmaDTO) {
        return turmaRepository.findByNome(turmaDTO.getNome());
    }

    public List<Turma> buscarTodos() {
        return turmaRepository.findAll();
    }

    public Page<TurmaDTO> buscarTodos(GenericFilter filter) {
        return turmaRepository.findAll(filter.getPageRequest()).map(TurmaDTO::new);
    }

    public TurmaDTO buscarPorId(Long id) {
        return turmaRepository.findById(id).map(TurmaDTO::new).orElse(null);
    }

    public void deletar(Long id) {
        turmaRepository.deleteById(id);
    }

    public void deletarTodosPorGradeHorariaId(Long id) {
        turmaRepository.deleteAllByGradeHorariaTesteId(id);
    }

    public TurmaDTO alterar(Long id, TurmaDTO turmaDTO) {
        if (!id.equals(turmaDTO.getId()))
            throw new GenericException("Erro ao atualizar o registro.");

        return salvarTurma(turmaDTO);
    }

    private TurmaDTO salvarTurma(TurmaDTO turmaDTO) {
        Turma turmaSalva = turmaRepository.save(TurmaDTO.toMapTurma(turmaDTO));

        return new TurmaDTO(turmaSalva);
    }
}
