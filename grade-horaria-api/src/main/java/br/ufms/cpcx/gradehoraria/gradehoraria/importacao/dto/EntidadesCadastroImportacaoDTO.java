package br.ufms.cpcx.gradehoraria.gradehoraria.importacao.dto;

import br.ufms.cpcx.gradehoraria.generico.utils.StringUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static br.ufms.cpcx.gradehoraria.generico.utils.ListaUtils.getListaSemRepeticoes;

@Data
public class EntidadesCadastroImportacaoDTO {
    private int indiceColunaTurma;
    private int indiceColunaProfessor;
    private int indiceColunaDisciplina;
    private int indiceColunaCargaHorariaSemananal;

    private List<String> turmas;
    private List<String> disciplinas;
    private List<String> professores;

    private Map<String, Long> mapIdsTurmasCadastradas;
    private Map<String, Long> mapIdsDisciplinasCadastradas;
    private Map<String, Long> mapIdsProfessoresCadastradas;

    public EntidadesCadastroImportacaoDTO() {
        this.turmas = new ArrayList<>();
        this.disciplinas = new ArrayList<>();
        this.professores = new ArrayList<>();
        this.mapIdsTurmasCadastradas = new HashMap<>();
        this.mapIdsDisciplinasCadastradas = new HashMap<>();
        this.mapIdsProfessoresCadastradas = new HashMap<>();
    }

    public void adicionarEntidadesCadastro(List<String> linhas) {
        for (int i = 1; i < linhas.size(); i++) {
            String[] colunas = linhas.get(i).split("\t");

            this.turmas.add(colunas[this.indiceColunaTurma]);
            this.disciplinas.add(colunas[this.indiceColunaDisciplina]);
            this.professores.add(colunas[this.indiceColunaProfessor]);
        }
    }

    public void limparRepeticoes() {
        this.turmas = getListaSemRepeticoes(this.turmas);
        this.disciplinas = getListaSemRepeticoes(this.disciplinas);
        this.professores = getListaSemRepeticoes(this.professores);
    }

    public void setIndicesColunasGradeHoraria(ImportacaoGradeHorariaDTO importacaoGradeHorariaDTO, String linhaCabecalho) {
        List<String> cabecalhos = StringUtils.getListaDoTexto(linhaCabecalho, "\t");

        this.indiceColunaTurma = cabecalhos.indexOf(importacaoGradeHorariaDTO.getColunaTurma());
        this.indiceColunaProfessor = cabecalhos.indexOf(importacaoGradeHorariaDTO.getColunaProfessor());
        this.indiceColunaDisciplina = cabecalhos.indexOf(importacaoGradeHorariaDTO.getColunaDisciplina());
        this.indiceColunaCargaHorariaSemananal = cabecalhos.indexOf(importacaoGradeHorariaDTO.getColunaCargaHorariaSemanal());
    }
}
