package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.AulaDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Setter
public class TurmaGradeHorariaDTO {
    private String turma;
    private Integer cargaHorariaTotal;
    private List<ProfessorGradeHorariaDTO> professores;

    public TurmaGradeHorariaDTO(String turma) {
        this.turma = turma;
        this.professores = new ArrayList<>();
    }

    public String getTurma() {
        return turma;
    }

    public void setCargaHorariaTotal(Integer cargaHorariaTotal) {
        this.cargaHorariaTotal = cargaHorariaTotal;
    }

    public Integer getCargaHorariaTotal() {
        int cargaHorariaTotal = this.professores.stream().mapToInt(ProfessorGradeHorariaDTO::getCargaHorariaTotal).sum();
        setCargaHorariaTotal(cargaHorariaTotal);
        return this.cargaHorariaTotal;
    }

    public Integer getCargaHorariaTotalProfesor(String professor) {
        setCargaHorariaTotal(this.professores.stream().filter(professorGradeHorariaDTO -> professorGradeHorariaDTO.equals(professor))
                .mapToInt(ProfessorGradeHorariaDTO::getCargaHorariaTotal).sum());
        return this.cargaHorariaTotal;
    }

    public List<String> getNomesProfessores() {
        return professores.stream().map(ProfessorGradeHorariaDTO::getProfessor).collect(Collectors.toList());
    }

    public List<ProfessorGradeHorariaDTO> getProfessores() {
        return professores;
    }

    public List<AulaDTO> getAulasProfessores() {
        List<AulaDTO> aulas = new ArrayList<>();

        professores.forEach(professorGradeHorariaDTO -> aulas.addAll(professorGradeHorariaDTO.getAulasEntidades()));

        return aulas;
    }

    public ProfessorGradeHorariaDTO adicionarProfessor(String professor) {
        ProfessorGradeHorariaDTO professorGradeHorariaDTO = new ProfessorGradeHorariaDTO(professor);

        this.professores.add(professorGradeHorariaDTO);

        return professorGradeHorariaDTO;
    }

    public ProfessorGradeHorariaDTO getProfessor(String professor) {
        List<ProfessorGradeHorariaDTO> professoresFiltrados = this.professores.stream().filter(professorGradeHorariaDTO -> professorGradeHorariaDTO.getProfessor().equals(professor)).collect(Collectors.toList());

        return !professoresFiltrados.isEmpty() ? professoresFiltrados.get(0) : null;
    }


    public List<EntidadeDTO> getEntidades() {
        List<EntidadeDTO> entidades = new ArrayList<>();

        this.professores.forEach(professorGradeHorariaDTO -> entidades.addAll(professorGradeHorariaDTO.getEntidades()));

        return entidades;
    }
}
