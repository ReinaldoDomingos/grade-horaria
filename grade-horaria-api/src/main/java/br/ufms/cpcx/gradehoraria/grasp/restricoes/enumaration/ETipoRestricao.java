package br.ufms.cpcx.gradehoraria.grasp.restricoes.enumaration;

public enum ETipoRestricao {
    DIAS_NAO_CONSECUTIVOS("Manter disciplina em dias alternados"),
    LOCAL_DISPONIVEL("Local disponível"),
    MESMO_DIA("Manter disciplinas mesmo dia"),
    NAO_USAR_MESMO_DIA("Semestres diferente"),
    DIAS_PROFESSOR_INDISPONIVEL("Semestres diferente");

    private final String valor;

    ETipoRestricao(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
