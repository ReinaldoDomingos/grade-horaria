package br.ufms.cpcx.gradehoraria.gradehoraria.repository;

import br.ufms.cpcx.gradehoraria.gradehoraria.dto.TurmaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Turma;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TurmaRepository extends JpaRepository<Turma, Long> {
    boolean existsTurmaByCodigo(String codigo);

    boolean existsTurmaByNome(String nome);

    void deleteAllByGradeHorariaTesteId(Long idGradeHoraria);

    TurmaDTO findByNome(String nome);
}
