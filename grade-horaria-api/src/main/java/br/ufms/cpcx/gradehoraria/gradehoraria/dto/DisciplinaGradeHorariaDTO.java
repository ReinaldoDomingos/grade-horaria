package br.ufms.cpcx.gradehoraria.gradehoraria.dto;

import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Disciplina;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.DisciplinaGradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.GradeHoraria;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Professor;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DisciplinaGradeHorariaDTO {

    private Long id;
    private Integer numero;
    private Integer semestre;
    private Professor professor;
    private Disciplina disciplina;
    private Integer cargaHorariaSemanal;
    private GradeHoraria gradeHoraria;

    public DisciplinaGradeHorariaDTO() {
    }

    public DisciplinaGradeHorariaDTO(DisciplinaGradeHoraria disciplinaGradeHoraria) {
        this.id = disciplinaGradeHoraria.getId();
        this.gradeHoraria = disciplinaGradeHoraria.getGradeHoraria();
        this.disciplina = disciplinaGradeHoraria.getDisciplina();
        this.semestre = disciplinaGradeHoraria.getSemestre();
        this.cargaHorariaSemanal = disciplinaGradeHoraria.getCargaHorariaSemanal();
    }
}
