package br.ufms.cpcx.gradehoraria.grasp.grasp.utils;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.AulaDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import br.ufms.cpcx.gradehoraria.grasp.grasp.dto.Candidato;
import br.ufms.cpcx.gradehoraria.grasp.grasp.grafo.Vertice;

import java.util.List;

public class MoverAulasDiaUtils {
    private MoverAulasDiaUtils() {
    }

    public static void moverCandidatosParaDia(int dia, List<Candidato<Vertice<Integer>, EntidadeDTO>> candidatos) {
        candidatos.forEach(candidato -> setDiaCandidato(dia, candidato));
    }

    public static void setDiaCandidato(int dia, Candidato<Vertice<Integer>, EntidadeDTO> candidato) {
        candidato.getValor().setCor(dia);
        candidato.getEntidade().getAulas().forEach(aulaDTO -> aulaDTO.setDia(String.valueOf(dia)));
    }

    public static void juntarCandidatosNoDia1(Candidato<Vertice<Integer>, EntidadeDTO> candidatoDia, Candidato<Vertice<Integer>, EntidadeDTO> candidatoOutroDia) {
        List<AulaDTO> aulasParaMover = candidatoOutroDia.getEntidade().getAulas();
        candidatoOutroDia.setValor(null);

        while (!aulasParaMover.isEmpty()) {
            candidatoDia.getEntidade().getAulas().add(aulasParaMover.remove(0));
            setDiaCandidato(candidatoDia.getValor().getCor(), candidatoDia);
        }
    }

    public static boolean ehPossivelMoverParaDia2(int cargaHorariaCandidatoDia1, int cargaHorariaCandidatoDia2,
                                                  Integer cargaHorariaProfessorNoDia2, Integer quantidadeAulasPorTurno) {
        return cargaHorariaCandidatoDia1 + cargaHorariaCandidatoDia2 <= quantidadeAulasPorTurno
                && cargaHorariaProfessorNoDia2 + cargaHorariaCandidatoDia1 <= quantidadeAulasPorTurno;
    }
}
