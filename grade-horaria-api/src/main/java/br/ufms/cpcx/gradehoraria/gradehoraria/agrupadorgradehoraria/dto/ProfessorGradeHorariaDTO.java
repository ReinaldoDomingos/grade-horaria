package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorgradehoraria.dto;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.AulaDTO;
import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

@Setter
public class ProfessorGradeHorariaDTO {
    private String professor;
    private List<EntidadeDTO> entidades;

    public ProfessorGradeHorariaDTO(String professor) {
        this.professor = professor;
        this.entidades = new ArrayList<>();
    }

    public String getProfessor() {
        return professor;
    }

    public List<EntidadeDTO> getEntidades() {
        if (isNull(this.entidades)) {
            return new ArrayList<>();
        }

        return this.entidades;
    }

    public List<AulaDTO> getAulasEntidades() {
        List<AulaDTO> aulas = new ArrayList<>();

        entidades.forEach(entidade -> aulas.addAll(entidade.getAulas()));

        return aulas;
    }

    public void adicionarEntidade(EntidadeDTO entidade) {
        this.entidades.add(entidade);
    }

    public Integer getCargaHorariaTotal() {
        return this.entidades.stream().mapToInt(EntidadeDTO::getCargaHorariaTotal).sum();
    }
}
