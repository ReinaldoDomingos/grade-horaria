package br.ufms.cpcx.gradehoraria.gradehoraria.agrupadorcargahoraria.dto;

import br.ufms.cpcx.gradehoraria.grasp.gradehoraria.dto.EntidadeDTO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Data
public class GradeCargaHorariaDTO {
    private List<Integer> dias;
    private final Integer quantidadeAulasPorTurno;
    private List<TurmaCargaHorariaDTO> turmas;

    public GradeCargaHorariaDTO(Integer quantidadeAulasPorTurno) {
        this.dias = new ArrayList<>();
        this.turmas = new ArrayList<>();
        this.quantidadeAulasPorTurno = quantidadeAulasPorTurno;
    }

    public void adicionarTurma(String turma) {
        this.turmas.add(new TurmaCargaHorariaDTO(turma));
    }

    private List<DiaCargaHorariaDTO> getDiasDisponiveisTurma(String professor, TurmaCargaHorariaDTO turmaCargaHorariaDTO, int cargaHorariaEntidade) {
        List<Integer> diasDisponiveis = new ArrayList<>(this.dias);

        for (Integer dia : this.dias) {
            int cargaHorariaProfessorDia = this.turmas.stream().mapToInt(turma -> turma.getCargaHorariaProfessorNoDia(dia, professor)).sum();
            int cargaHorariaDisponivelProfessor = this.quantidadeAulasPorTurno - cargaHorariaProfessorDia;

            if (cargaHorariaProfessorDia >= this.quantidadeAulasPorTurno || cargaHorariaDisponivelProfessor < cargaHorariaEntidade) {
                diasDisponiveis.remove(dia);
            }
        }

        return turmaCargaHorariaDTO.getDias().stream()
                .filter(diaCargaHorariaDTO -> diasDisponiveis.contains(diaCargaHorariaDTO.getDia()))
                .filter(diaCargaHorariaDTO -> diaCargaHorariaDTO.getCargaHorariaDisponivelTurma() > 0)
                .collect(Collectors.toList());
    }

    public void adicionarEntidade(EntidadeDTO entidade) {
        adicionarEntidade(entidade, null);
    }

    public void adicionarEntidade(EntidadeDTO entidade, Integer diaInicial) {
        TurmaCargaHorariaDTO turmaCargaHorariaDTO = getTurma(entidade.getSemestre());

        List<DiaCargaHorariaDTO> diasDisponiveis = getDiasDisponiveisTurma(entidade.getProfessor(), turmaCargaHorariaDTO, entidade.getCargaHorariaTotal())
                .stream().filter(diaCargaHorariaDTO -> isNull(diaInicial) || diaCargaHorariaDTO.getDia() >= diaInicial).collect(Collectors.toList());

        if (diasDisponiveis.isEmpty()) {
            int novoDia = this.dias.size() + 1;
            this.dias.add(novoDia);
            this.turmas.forEach(turma -> turma.adicionarDia(novoDia, this.quantidadeAulasPorTurno));
            diasDisponiveis.add(turmaCargaHorariaDTO.getDia(novoDia));
        }

        diasDisponiveis.get(0).adicionarEntidade(entidade);
    }

    private TurmaCargaHorariaDTO getTurma(String turma) {
        return this.turmas.stream()
                .filter(turmaCargaHorariaDTO -> turmaCargaHorariaDTO.getTurma().equals(turma))
                .collect(Collectors.toList()).get(0);
    }

    public void adicionarDias(int qtdDias, Integer quantidadeAulasPorTurno) {
        for (int i = 0; i < qtdDias; i++) {
            this.dias.add(i + 1);
        }

        this.turmas.forEach(turmaCargaHorariaDTO -> turmaCargaHorariaDTO.adicionarDias(qtdDias, quantidadeAulasPorTurno));
    }

    public void adicionarTurmas(List<String> turmas) {
        turmas.forEach(this::adicionarTurma);
    }
}
