package br.ufms.cpcx.gradehoraria.arquivos.business;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import static java.util.Objects.requireNonNull;

@Component
public class ArquivosBO {

    @Value("${gradehoraria.disco.raiz}")
    private String raiz;

    @Value("${gradehoraria.disco.diretorio-xls}")
    private String diretorioXLS;

    public String getURI(String nomeArquivo) {
        Path diretorioPath = Paths.get(this.raiz, this.diretorioXLS);
        Path arquivoPath = diretorioPath.resolve(nomeArquivo);

        return arquivoPath.getParent() + "/" + nomeArquivo;
    }

    public String salvarXLS(MultipartFile arquivo) {
        return salvar(this.diretorioXLS, arquivo);
    }

    public String salvar(String diretorio, MultipartFile arquivo) {
        Path diretorioPath = Paths.get(this.raiz, diretorio);
        String nomeOrginalArquivo = arquivo.getOriginalFilename();
        String nomeArquivoSalvo = gerarNomeArquivo(nomeOrginalArquivo);
        Path arquivoPath = diretorioPath.resolve(nomeArquivoSalvo);

        try {
            Files.createDirectories(diretorioPath);
            arquivo.transferTo(arquivoPath.toFile());
        } catch (IOException e) {
            throw new RuntimeException("Problemas na tentativa de salvar arquivo.", e);
        }

        return nomeArquivoSalvo;
    }

    private String gerarNomeArquivo(String nomeOrginalArquivo) {
        nomeOrginalArquivo = nomeOrginalArquivo.replaceAll(".xlsx", "").replaceAll(".xls", "");
        return requireNonNull(nomeOrginalArquivo) + new Date().getTime() + ".xlsx";
    }
}
