package br.ufms.cpcx.gradehoraria.gradehoraria.controller;

import br.ufms.cpcx.gradehoraria.generico.exception.GenericException;
import br.ufms.cpcx.gradehoraria.gradehoraria.dto.TurmaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Turma;
import br.ufms.cpcx.gradehoraria.gradehoraria.filter.GradeHorariaFilter;
import br.ufms.cpcx.gradehoraria.gradehoraria.service.TurmaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Controller
@RestController
@RequestMapping("/gradehoraria-api/turma")
public class TurmaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TurmaController.class);

    @Autowired
    private TurmaService turmaService;

    @GetMapping
    public Page<TurmaDTO> buscar(@RequestParam Map<String, String> filters) {
        return turmaService.buscarTodos(GradeHorariaFilter.of(filters));
    }

    @GetMapping("/todas")
    public List<Turma> buscarTodas() {
        return turmaService.buscarTodos();
    }

    @GetMapping("/{id}")
    public TurmaDTO buscarPorId(@PathVariable("id") Long id) {
        return turmaService.buscarPorId(id);
    }

    @PostMapping
    public TurmaDTO salvar(@RequestBody TurmaDTO turmaDTO) {
        return turmaService.salvar(turmaDTO);
    }

    @DeleteMapping("{id}")
    public void deletar(@PathVariable("id") Long id) {
        try {
            turmaService.deletar(id);
        } catch (Exception e) {
            LOGGER.error("Erro ao excluir registro.", e);
            throw new GenericException("Erro ao excluir registro.");
        }
    }

    @PutMapping("{id}")
    public TurmaDTO alterar(@PathVariable("id") Long id, @RequestBody TurmaDTO turmaDTO) {
        return turmaService.alterar(id, turmaDTO);
    }
}
