package br.ufms.cpcx.gradehoraria.gradehoraria.repository;

import br.ufms.cpcx.gradehoraria.gradehoraria.dto.DisciplinaDTO;
import br.ufms.cpcx.gradehoraria.gradehoraria.entity.Disciplina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DisciplinaRepository extends JpaRepository<Disciplina, Long> {
    boolean existsDisciplinaByCodigo(String codigo);

    boolean existsDisciplinaByNome(String nome);

    DisciplinaDTO findByNome(String nome);

    void deleteAllByGradeHorariaTesteId(Long idGradeHoraria);
}
